#!/usr/bin/env bash

rm -rf bin && mkdir bin
cd bin
wget https://raw.githubusercontent.com/jpdev0/laravel-env/master/dependencies-init.sh -O dependencies-init.sh
sudo bash dependencies-init.sh
