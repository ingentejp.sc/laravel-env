#!/usr/bin/env bash
# if dependencies is not working just install it manually
# just go to the line it all works and tested

# update repository
sudo apt-get update

# install http tools
sudo apt-get install wget unzip git curl -y

# install laravel php and php-extensions
sudo apt-get install php7.2 php7.2-common php7.2-cli php7.2-gd php7.2-curl php7.2-intl php7.2-mbstring php7.2-bcmath php7.2-imap php7.2-xml php7.2-zip php7.2-fpm -y

# install composer
curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

# install nodejs
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install nodejs -y

# install nginx
sudo apt-get install -y nginx
